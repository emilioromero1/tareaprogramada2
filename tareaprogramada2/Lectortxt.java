import java.io.*;

/**
 * Write a description of class Lectortxt here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Lectortxt
{
    public void lectorTxt(String direction)//direccion del archivo
    {
        String txt = "";
        try{
            BufferedReader bf= new BufferedReader(new FileReader(direction));
            String bfRead;
            String temp="";
            while((bfRead = bf.readLine())!=null)//ciclo mientras bfRead tenga datos
            {
                temp = temp + bfRead;//guardar texto
            }
            txt = temp;
        }catch(Exception e)
        {
            System.out.println("No se encontró el archivo de tipo texto");
        }
        return txt;
    }
     
}
